**Running AH-HA code**

*syntax:*
	Rscript forensic_dna60.R *<REF_HAPS> <CHUNK_LENGTH> <MIX_NAME> <CHR>*

---

## Commandline Parameters

1. **REF_HAPS:** number of reference haplotypes to be used
2. **CHUNK_LENGTH:** length of each chunk, in 1e6 Morgans, used in parallel 
3. **MIX_NAME:** "mix ratio" of the mixture sample file name to be used
4. **CHR:** chromosome number to be used

---

## Coded parameters

1. **EM:** use EM generated model coefficients? - True\False
2. **Theta:** Theta coefficient mangnitude
3. **Neff:** Neff coefficient magnitude
4. **Eps:** Epsilon coefficient magnitude
5. **OUT:** Output file name header
6. **OutPATH:** Output file path
7. **MixFile:** mixture file name and path - eg.: paste0("./AJ_trio/AJ_father_mother.",CHR,".mixed_dna.",MIX_NAME,".hap")

---

## Input Files

[all with no column headers]

1. **ref_hap:** population specific haplotypes. each column represents a haplotype. format- ".hap", values- 0\1 = REF\ALT SNP
2. **gen_dis:** ".map" file, distance of relevant SNPs in Morgans. columns- CHR | Name | dist | SNP Position
3. **mix_legend:** ".legend" file, of the mixture sample. columns- Name | Position | REF | ALT
4. **EM_table:** population specific coefficients, EM estimated from reference haplotypes - no header - columns: chrom | Neff | Theta
5. **mixed_geno:** A file with two cloumns - the number of reads of allele "a" and the number of reads of  allele "b". The SNPs need to be in the same order as in the ref_hap file.
6. **vic_geno:** A file with two columns of phased haplotypes with the same format as in the ref_hap file.The SNPs need to be in the same order as in the ref_hap file.

---

## Output
1. **Path:** OutPATH
2. **OUT_HEADER:** OUT+MIX_NAME+CHR+REFS+CHUNK_LENGTH+Iteration_NUM
3. **Geno Output file:** estimated genotype and emission probabilities - values: 0\1\2, columns: SH-HMM | Next-SNP | AH-HA | AA_prob | AB_prob | BB_prob 	
4. **Log file:** ".log"

